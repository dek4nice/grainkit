# Mixin library for sass+pug and small boilerplate

## Vendor prefixes
The mixins contain vendor prefixes for support in all browsers.

## Write CSS in fastest way
The library contains mixins, functions, and other addons.

## Settings
$state_underconstruction: grayscale colours, disabled animations
$state_publish: rem-calc sizes instead pixels, vendor prefixes for CSS

